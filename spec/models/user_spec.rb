require 'spec_helper'

describe User do


	describe "profile page" do
		let(:user) { FactoryGirl.create(:user)}
		before { visit user}

		it { should have_content(user.name) }
		it { should have title(user.name) }
		it { should respond_to(:followed_users) }
	end

	describe "sign_up page" do
		before { visit sign_up_path } 

		it { should have_content('Sign up') }
		it { should have_title(full_title('Sign up'))}
	end

	it { should respond_to(:password_confirmation) }
	it { should respond_to(:remember_token) }
    it { should respond_to(:authenticate) }
end
