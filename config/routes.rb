LxApp::Application.routes.draw do

  resources :users do
	  member do
		  get :following, :followers
	  end
  end

  resources :sessions,      only: [:new, :create, :destroy] 
  resources :microposts,    only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]

  match '/',        to: 'static_pages#home' 
  match '/help',    to: 'static_pages#help'
  match '/about',   to: 'static_pages#about'
  match '/contact', to: 'static_pages#contact'
  match '/signup',  to: 'users#new' , via: 'get'
  match '/signin',  to: 'sessions#new' , via: 'get'
  match '/signout', to: 'sessions#destroy' , via: 'delete'

  root :to => 'static_pages#home'

end
